#include "widget.h"

#include <QGuiApplication>
#include <QKeyEvent>
#include <QPainter>

Widget::Widget(QWidget *parent)
    : QWidget(parent)
{
    resize(m_itemLeftPadding + m_itemRightPadding + m_itemSize.width(),
           m_itemSize.height() * m_itemCount);
}

Widget::~Widget()
{
}

QRect Widget::itemGeometry(int item) const
{
    return QRect(m_itemLeftPadding, m_itemSize.height() * item, m_itemSize.width(), m_itemSize.height());
}

void Widget::increment()
{
    const int oldItem = m_currentItem;
    m_currentItem++;
    if (m_currentItem >= m_itemCount) {
        m_currentItem = m_itemCount - 1;
    }
    update(QRegion(itemGeometry(oldItem).united(itemGeometry(m_currentItem))));
}

void Widget::decrement()
{
    const int oldItem = m_currentItem;
    m_currentItem--;
    if (m_currentItem < 0) {
        m_currentItem = 0;
    }
    update(QRegion(itemGeometry(oldItem).united(itemGeometry(m_currentItem))));
}

void Widget::keyPressEvent(QKeyEvent *event)
{
    switch (event->key()) {
    case Qt::Key_Up:
        decrement();
        event->accept();
        break;
    case Qt::Key_Down:
        increment();
        event->accept();
        break;
    case Qt::Key_Escape:
        qGuiApp->quit();
        event->accept();
        break;
    default:
        QWidget::keyPressEvent(event);
    }
}

void Widget::mousePressEvent(QMouseEvent *event)
{
    switch (event->button()) {
    case Qt::LeftButton:
        decrement();
        event->accept();
        break;
    case Qt::RightButton:
        increment();
        event->accept();
        break;
    default:
        QWidget::mousePressEvent(event);
    }
}

void Widget::paintEvent(QPaintEvent *event)
{
    Q_UNUSED(event)

    QPainter painter(this);
    for (int i = 0; i < m_itemCount; ++i) {
        const QColor color = i == m_currentItem ? Qt::red : Qt::blue;
        painter.fillRect(itemGeometry(i), color);
    }
}
