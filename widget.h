#pragma once

#include <QWidget>

class Widget : public QWidget
{
    Q_OBJECT

public:
    Widget(QWidget *parent = nullptr);
    ~Widget();

protected Q_SLOTS:
    void paintEvent(QPaintEvent *event) override;
    void keyPressEvent(QKeyEvent *event) override;
    void mousePressEvent(QMouseEvent *event) override;

private:
    void increment();
    void decrement();
    QRect itemGeometry(int item) const;

    int m_currentItem = 0;
    int m_itemCount = 40;
    const QSize m_itemSize = QSize(229, 21);
    const int m_itemLeftPadding = 1;
    const int m_itemRightPadding = 1;
};
